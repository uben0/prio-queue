# Priority Queue

A simple priority queue implemented in Rust as a binary heap stored in a `Vec<T>`.

This implementation aims to be simple and more flexible than the one
provided by the standard library. It relies on a given function to determine
priority between elements. Let you acces the underlying `Vec<T>` with
consistency check or not.


## Example
```rust
let mut queue = PrioQueue::new(|l, r| l < r);
queue.push(42);
queue.push(32);
queue.push(64);
assert_eq!(queue.pop(), Some(32));
assert_eq!(queue.pop(), Some(42));
assert_eq!(queue.pop(), Some(64));
assert_eq!(queue.pop(), None);
```

## Display
```
              ╭───────────────9───────────────╮             
      ╭───────26──────╮               ╭───────27──────╮     
  ╭───45──╮       ╭───34──╮       ╭───35──╮       ╭───37──╮ 
╭─59╮   ╭─52╮   ╭─48      57      67      39      80      73
77  61  64  74  96                                          
```
